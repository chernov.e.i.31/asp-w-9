
import { connect, useSelector } from "react-redux";
import { useParams } from "react-router";
import { User } from "../Reducers/userReducer";
import React from "react";

interface Props {
    newUser?: User;
}

const NewUser = (props: Props) => {
    const { newUser } = props;

    return newUser && 
        <div>
            <label>Зарегистрирован новый пользователь</label>
            <div >
                {newUser?.fullName}
            </div >
        </div>;
}

const mapStateToProps = (state: any) => {
    console.log(state);
    return { newUser: state.user.user };
}

export default connect(mapStateToProps)(NewUser);