import { configureStore } from "@reduxjs/toolkit";
import userReducer from "../Reducers/userReducer.ts";


const store = configureStore({
    reducer: {
      user: userReducer,
    },
});

export default store;