import './App.css';
import Login from './Login/Login.tsx';
import Home from './HomePage/Home.tsx';
import { Route, Routes, Link } from 'react-router-dom';
import React from 'react';
import Registration from './Registration/Registration.tsx';
import Weather from './Weather/Weather.tsx';



function App() {
  return (<>
      <nav className="navbar navbar-expand-lg bg-body-tertiary">
        <div className="container-fluid">
          <div className="navbar-nav">
              <a className="nav-link" href="/">Главная</a>
              <a className="nav-link" href="/login">Войти</a>
              <a className="nav-link" href="/registration">Регистрация</a>
              <a className="nav-link" href="/weather">Погода</a>
          </div>
        </div>
      </nav>
      <Routes>
        <Route index element={<Home />} />
        <Route path="login" element={<Login />} />
        <Route path="registration" element={<Registration />} />
        <Route path="weather" element={<Weather />} />
        <Route path="*" element={<span>404</span>} />
      </Routes>
    </>
  );
}

export default App;
