export interface RegistrationState {
    name: string;
    surname: string;
    password: string;
    passwordConfirm: string;
    result: any;
}