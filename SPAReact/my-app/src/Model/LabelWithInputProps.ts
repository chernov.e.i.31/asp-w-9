export interface LabelWithInputProps {
    labelText: string;
    inputType: string;
    onChange: any;
}