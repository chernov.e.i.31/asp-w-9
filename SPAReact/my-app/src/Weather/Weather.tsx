import React, { Component } from "react";

export default class Weather extends Component {
    constructor(props:{})
    {
        super(props);
        this.state = { forecasts: []};
    }

    componentDidMount() {
        this.getData();
    }

    async getData () {
        var data = await fetch('https://localhost:7128/WeatherForecast');
        this.setState({ forecasts: await data.json() })
    }

    static renderWeather(data) {
        return <table className='table table-striped' aria-labelledby="tabelLabel">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Temp. (C)</th>
                    <th>Temp. (F)</th>
                    <th>Summary</th>
                </tr>
            </thead>
            <tbody>
                {data.map(forecast =>
                    <tr key={forecast.date}>
                    <td>{forecast.date}</td>
                    <td>{forecast.temperatureC}</td>
                    <td>{forecast.temperatureF}</td>
                    <td>{forecast.summary}</td>
                    </tr>
                )}
            </tbody>
        </table>
    }

    render () {
        let contents = Weather.renderWeather(this.state.forecasts);
        return (
            <>
                {contents}
            </>);
    }
}