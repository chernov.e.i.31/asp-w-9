import React from "react";
import { Component } from "react";
import { ButtonProps } from "../Model/ButtonProps";

export default class ButtonAction extends Component<ButtonProps> {
    render() {
        return <>
            <div className="row justify-content-center">
                <button type="button" className="btn btn-primary" onClick={this.props.onClick}>{this.props.text}</button>
            </div>
        </>
    }
}